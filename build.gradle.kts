val kotlinVersion = "1.3.21"
val kotlinFrontendVersion = "0.0.45"
val ktorVersion = "1.1.3"
val logbackVersion = "1.2.3"

plugins {
    kotlin("multiplatform") version "1.3.21"
//    id("org.jetbrains.kotlin.frontend") version "0.0.45"
//    id("kotlin2js")
}
repositories {
    jcenter()
    mavenCentral()
}
dependencies {
    kotlinCompilerClasspath("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
    kotlinCompilerClasspath("org.jetbrains.kotlin:kotlin-frontend-plugin:$kotlinFrontendVersion")
}

group = "com.oleg"
version = "0.0.1"

kotlin {
    jvm()
    jvm("api")
    js()
    mingwX64("mingw")
    sourceSets {
        val commonMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-stdlib-common")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-test-common")
                implementation("org.jetbrains.kotlin:kotlin-test-annotations-common")
            }
        }
        val jvmMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-test")
                implementation("org.jetbrains.kotlin:kotlin-test-junit")
            }
        }
        val apiMain by getting {
            dependencies {
                dependsOn(jvmMain)
                implementation("io.ktor:ktor-server-core:$ktorVersion")
                implementation("io.ktor:ktor-server-netty:$ktorVersion")
                implementation("io.ktor:ktor-gson:$ktorVersion")
                implementation("ch.qos.logback:logback-classic:$logbackVersion")
            }
        }
        val apiTest by getting {
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-test")
                implementation("org.jetbrains.kotlin:kotlin-test-junit")
            }
        }
        val jsMain by getting {
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-stdlib-js")
            }
        }
        val jsTest by getting {
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-test-js")
            }
        }
        val mingwMain by getting {
        }
        val mingwTest by getting {
        }
    }
}