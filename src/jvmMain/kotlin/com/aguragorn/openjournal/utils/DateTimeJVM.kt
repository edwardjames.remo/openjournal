package com.aguragorn.openjournal.utils

import com.aguragorn.openjournal.di.Provider
import java.time.Instant
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

/**
 * Should always be in UTC
 */
actual class DateTime private actual constructor() {
    internal var dateTimeValue: ZonedDateTime = ZonedDateTime.now(ZoneOffset.UTC)

    actual var year: Int
        get() = dateTimeValue.year
        set(value) {
            dateTimeValue = dateTimeValue.withYear(value)
        }
    actual var month: Int
        get() = dateTimeValue.monthValue
        set(value) {
            dateTimeValue = dateTimeValue.withMonth(value)
        }
    actual var day: Int
        get() = dateTimeValue.dayOfMonth
        set(value) {
            dateTimeValue = dateTimeValue.withDayOfMonth(value)
        }
    actual var hour: Int
        get() = dateTimeValue.hour
        set(value) {
            dateTimeValue = dateTimeValue.withHour(value)
        }
    actual var minute: Int
        get() = dateTimeValue.minute
        set(value) {
            dateTimeValue = dateTimeValue.withMinute(value)
        }
    actual var second: Int
        get() = dateTimeValue.second
        set(value) {
            dateTimeValue = dateTimeValue.withSecond(value)
        }
    actual var nano: Int
        get() = dateTimeValue.nano
        set(value) {
            dateTimeValue = dateTimeValue.withNano(value)
        }


    /**
     * Converts the [DateTime] to milliseconds from epoch date.
     */
    actual fun toEpochMillis(): Long = dateTimeValue.toInstant().toEpochMilli()

    /**
     * Converts the [DateTime] to a string formatted by user preference.
     * The conversion considers the users date format and the timezone preferences.
     */
    actual fun toUserPreferredFormat(): String {
        val format: String = Provider.singleton(PROVIDER_ID_DATE_TIME_FORMAT)
        val timezone: String = Provider.singleton(PROVIDER_ID_TIMEZONE)
        val timezoneAdjustedDateTime = dateTimeValue.withZoneSameInstant(ZoneOffset.of(timezone))
        return DateTimeFormatter.ofPattern(format).format(timezoneAdjustedDateTime)
    }

    actual companion object {
        @Suppress("MayBeConstant")
        actual val PROVIDER_ID_DATE_TIME_FORMAT = "com.aguragorn.openjournal.utils.PROVIDER_ID_DATE_TIME_FORMAT"
        @Suppress("MayBeConstant")
        actual val PROVIDER_ID_TIMEZONE = "com.aguragorn.openjournal.utils.PROVIDER_ID_TIMEZONE"

        /**
         * Creates an instance of the [DateTime] for the current date and time
         */
        actual fun now(): DateTime = DateTime()

        /**
         * Creates a [DateTime] instance from milliseconds since epoch date.
         */
        actual fun from(epochMillis: Long): DateTime = DateTime().apply {
            dateTimeValue = ZonedDateTime.ofInstant(Instant.ofEpochMilli(epochMillis), ZoneOffset.UTC)
        }
    }


}