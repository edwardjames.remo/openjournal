package com.aguragorn.openjournal.utils

import java.util.*

actual fun String.Companion.randomGuid(): String = UUID.randomUUID().toString()