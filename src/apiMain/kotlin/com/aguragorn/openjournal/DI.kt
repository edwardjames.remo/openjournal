package com.aguragorn.openjournal

import com.aguragorn.openjournal.di.Provider
import com.aguragorn.openjournal.notes.api.NotesController
import com.aguragorn.openjournal.notes.api.notesRoutingConf
import com.aguragorn.openjournal.notes.datasource.NotesDataSource
import com.aguragorn.openjournal.notes.datasource.NotesRepository
import com.aguragorn.openjournal.utils.DateTime

fun setupDI() {
    Provider.apply {
        // Routing
        register(id = "notesRouting") { notesRoutingConf }

        // Controller
        register { NotesController() }

        // Data Source
        register { NotesRepository() as NotesDataSource }

        // Config
        register(id = DateTime.PROVIDER_ID_DATE_TIME_FORMAT) { "MMM dd, HH:mm" }
        register(id = DateTime.PROVIDER_ID_TIMEZONE) { "+00:00" }
    }
}