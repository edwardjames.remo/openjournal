package com.aguragorn.openjournal.notes.datasource

import com.aguragorn.openjournal.notes.model.Note
import com.aguragorn.openjournal.utils.randomGuid

class NotesRepository : NotesDataSource {
    @Deprecated("Replace with proper db")
    private val notes: List<Note> = listOf(
        Note(
            id = String.randomGuid(),
            title = "First Note"
        ),
        Note(
            id = String.randomGuid(),
            title = "Another Note"
        )
    )

    override suspend fun save(note: Note) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun getAll(): List<Note> {
        return notes
    }

    override suspend fun delete(note: Note): Note? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}