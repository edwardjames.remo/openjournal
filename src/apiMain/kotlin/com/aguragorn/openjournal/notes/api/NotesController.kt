package com.aguragorn.openjournal.notes.api

import com.aguragorn.openjournal.di.Provider
import com.aguragorn.openjournal.notes.datasource.NotesDataSource
import com.aguragorn.openjournal.notes.model.Note
import kotlinx.coroutines.runBlocking

class NotesController(
    private val notesDataSource: NotesDataSource = Provider.singleton()
) {
    fun getNotes(): List<Note> = runBlocking {
        return@runBlocking notesDataSource.getAll()
    }
}