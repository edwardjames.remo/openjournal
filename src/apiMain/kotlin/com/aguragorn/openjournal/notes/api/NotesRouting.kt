package com.aguragorn.openjournal.notes.api

import com.aguragorn.openjournal.RoutingConf
import com.aguragorn.openjournal.di.Provider
import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.get


val notesRoutingConf: RoutingConf = {
    val notesController: NotesController = Provider.singleton()

    get("notes") {
        call.respond(notesController.getNotes())
    }
}