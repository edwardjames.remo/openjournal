package com.aguragorn.openjournal

import com.aguragorn.openjournal.di.Provider
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.gson.gson
import io.ktor.routing.Routing
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

typealias RoutingConf = Routing.() -> Unit

fun main() {
    setupDI()

    embeddedServer(Netty, 8080) {
        install(ContentNegotiation) {
            gson { }
        }
        routing {
            val routings = Provider.singletonsOf<RoutingConf>()
            routings.forEach { it.invoke(this) }
        }
    }.start(wait = true)
}
