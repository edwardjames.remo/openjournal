package com.aguragorn.openjournal.notes.datasource

import com.aguragorn.openjournal.notes.model.Note

interface NotesDataSource {
    /**
     * If a note with the same [Note.id] exists then it will be updated, else
     * the [note] will be added as new.
     */
    suspend fun save(note: Note)

    /**
     * @return all existing [Note]
     */
    suspend fun getAll(): List<Note>

    /**
     * Deletes an existing [Note] with the same [Note.id] as [note]
     * @return the actual [Note] removed from the data source, `null` if no note was deleted
     */
    suspend fun delete(note: Note): Note?
}