package com.aguragorn.openjournal.notes.model

import com.aguragorn.openjournal.utils.DateTime
import com.aguragorn.openjournal.utils.randomGuid

/**
 * A tag which can be used to label a [Note].
 *
 * @property id Used to identify the specific tag
 * @property name The name of the tag that will be visible to the user
 * @property value a value associated to a [Tag]
 */
open class Tag<T>(
    var id: String = String.randomGuid(),
    var name: String,
    open var value: T? = null
) where T : Any

class DateTimeTag(
    value: DateTime = DateTime.now()
) : Tag<DateTime>(
    id = "com.aguragorn.openjournal.notes.DateTimeTagId",
    name = "DateTime",
    value = value
)

class TaskTag : Tag<Boolean>(
    id = "com.aguragorn.openjournal.notes.TaskTagId",
    name = "Task"
) {
    var isDone: Boolean
        get() = this.value == true
        set(value) {
            this.value = value
        }
}

class RegularTag(name: String) : Tag<Any>(name = name)
