package com.aguragorn.openjournal.notes.model

import com.aguragorn.openjournal.utils.randomGuid

class Note(
    var id: String = String.randomGuid(),
    var title: String = "",
    var content: String = "",
    var tags: List<Tag<*>> = listOf()
)