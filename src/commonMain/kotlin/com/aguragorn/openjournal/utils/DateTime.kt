package com.aguragorn.openjournal.utils

/**
 * Platform agnostic date object. Should always be in UTC.
 */
expect class DateTime private constructor() {
    var year: Int
    /**
     * 1 (January) to 12 (December)
     */
    var month: Int
    /**
     * 1 to 31
     */
    var day: Int
    var hour: Int
    var minute: Int
    var second: Int
    /**
     * 0 to 999,999,999
     */
    var nano: Int

    /**
     * Converts the [DateTime] to milliseconds from epoch date.
     */
    fun toEpochMillis(): Long

    /**
     * Converts the [DateTime] to a string formatted by user preference.
     * The conversion considers the users date format and the timezone preferences.
     */
    fun toUserPreferredFormat(): String

    companion object {
        val PROVIDER_ID_DATE_TIME_FORMAT: String
        val PROVIDER_ID_TIMEZONE: String
        /**
         * Creates an instance of the [DateTime] for the current date and time
         */
        fun now(): DateTime

        /**
         * Creates a [DateTime] instance from milliseconds since epoch date.
         */
        fun from(epochMillis: Long): DateTime
    }
}