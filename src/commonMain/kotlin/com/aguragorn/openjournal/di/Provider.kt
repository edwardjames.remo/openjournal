package com.aguragorn.openjournal.di

/**
 * Keeps track of registered implementations for all Types
 */
class Provider<T>
@PublishedApi
internal constructor(val create: () -> T) {

    fun instance(): T = create()

    companion object Factory {
        @PublishedApi
        internal val providerMap = mutableMapOf<InstanceIdentifier, Provider<*>>()

        @PublishedApi
        internal val singletonMap = mutableMapOf<InstanceIdentifier, Any>()

        /**
         * Registers an instance creator as a provider. A unique [id] can be supplied for multiple
         * creators for the same type. If multiple creators with the same id (or if no ids were supplied)
         * are registered, the last creator will always be used.
         *
         * ### Usage
         *     interface A
         *     class B : A
         *     class C : A
         *     Provider.register { arg -> B() as A }
         *     Provider.register(id = "uniqueID") { arg -> C() as A }
         * @param id unique identifier for the creator being registered. Can be null.
         * @param create a lambda expression that will create an instance of the requested type [T].
         * @param T the type associated to the creator being registered.
         * @since 0.0.1
         */
        inline fun <reified T> register(id: Any? = null, noinline create: () -> T) {
            val identifier = InstanceIdentifier(T::class, id)
            providerMap[identifier] = Provider(create)
        }

        /**
         * Returns an instance of type [T] created using a compatible registered provider. An [id] can be used to retrieve
         * an instance from a specific creator. Calls to this method will always result in a new object instance.
         *
         * To ensure testability of your classes use this to provide a default value on the constructor of
         * your class instead of providing a value to a property. This leaves the option to pass a mock instance
         * for testing.
         *
         * ### Usage
         *     // without id
         *     class MyClass(private val a: A = Provider.newInstance())
         *     // With id
         *     class MyClass(private val a: A = Provider.newInstance("uniqueID"))
         *
         * @param id unique object that can be used to determine the specific provider to use in creating an instance.
         * Can be null.
         * @param T the type to get an instance of.
         * @return an instance of type [T] created by a registered provider. Always a new instance for every call.
         * @throws NoProviderFoundException when no provider has been registered for the combination of the requested
         * type [T] and [id]
         * @throws InstanceCreationFailedException if the creator registered fails or returns `null`
         * @since 0.0.1
         */
        inline fun <reified T> newInstance(id: Any? = null): T {
            val identifier = InstanceIdentifier(T::class, id)
            val provider =
                providerMap[identifier] ?: throw NoProviderFoundException("No provider found for ${T::class}")

            val instance = try {
                provider.instance()
            } catch (e: Throwable) {
                throw InstanceCreationFailedException("instance creation failed for ${T::class}", e)
            }

            return instance as T ?: throw InstanceCreationFailedException("provider returned null for ${T::class}")
        }

        /**
         * @return a list of new instances from all registered creators for type [T]
         * @param T the type to get instances of
         * @since 0.0.1
         */
        inline fun <reified T> newInstancesOf(): List<T> = providerMap.asSequence()
            .filter { (key, _) -> key.type == T::class }
            .map { it.key }
            .map { newInstance(it.id) as T }
            .toList()

        /**
         * Returns an instance of type [T] created using a compatible registered provider. An [id] can be used to
         * retrieve an instance from a specific creator. Calls to this method will always return the same object
         * instance.
         *
         * To ensure testability of your classes use this to provide a default value on the constructor of
         * your class instead of providing a value to a property. This leaves the option to pass a mock instance
         * for testing.
         *
         * ### Usage
         *     // without id
         *     class MyClass(private val a: A = Provider.singleton())
         *     // With id
         *     class MyClass(private val a: A = Provider.singleton("uniqueID"))
         *
         * @param id unique object that can be used to determine the specific instance to retrieve. Can be null.
         * @param T the type to get an instance of.
         * @return an instance of type [T] created by a registered provider. Always the same instance for every call.
         * @throws NoProviderFoundException when no provider has been registered for the combination of the requested
         * type [T] and [id]
         * @throws InstanceCreationFailedException if the creator registered fails or returns `null`
         * @since 0.0.1
         */
        inline fun <reified T : Any> singleton(id: Any? = null): T {
            val identifier = InstanceIdentifier(T::class, id)
            return singletonMap[identifier] as? T ?: run {
                val instance = newInstance<T>(id)
                singletonMap[identifier] = instance
                instance
            }
        }

        /**
         * @return a list of new instances from all registered creators for type [T]
         * @param T the type to get instances of
         * @since 0.0.1
         */
        inline fun <reified T : Any> singletonsOf(): List<T> = providerMap.asSequence()
            .filter { (key, _) -> key.type == T::class }
            .map { it.key }
            .map { singleton(it.id) as T }
            .toList()
    }
}

@PublishedApi
internal data class InstanceIdentifier(val type: Any, val id: Any?)

class NoProviderFoundException(msg: String) : Exception(msg)
class InstanceCreationFailedException(msg: String, cause: Throwable? = null) : Exception(msg, cause)