package com.aguragorn.openjournal.di

import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertTrue
import kotlin.test.fail

@Suppress("FunctionName")
class ProviderTests {

    @BeforeTest
    fun setup() {
        Provider.register { FakeClass() as Fake }
    }

    @Test
    fun `newInstance() should return an instance when the type is registered`() {
        try {
            @Suppress("UNUSED_VARIABLE")
            val fake: Fake = Provider.newInstance()

            assertTrue(true)
        } catch (e: Exception) {
            fail("Fake should be injected properly")
        }
    }

    @Test
    fun `newInstance() should return different instance per call`() {
        val fake1: Fake = Provider.newInstance()
        val fake2: Fake = Provider.newInstance()

        assertTrue(fake1 != fake2, "newInstance returned the same instance")
    }

    @Test
    fun `singleton() should return an instance when the type is registered`() {
        try {
            @Suppress("UNUSED_VARIABLE")
            val fake1: Fake = Provider.singleton()

            assertTrue(true)
        } catch (e: Exception) {
            fail("Fake should be injected properly")
        }
    }


    @Test
    fun `singleton() should return same instance per call`() {
        val fake1: Fake = Provider.singleton()
        val fake2: Fake = Provider.singleton()

        @Suppress("ReplaceAssertBooleanWithAssertEquality")
        assertTrue(fake1 == fake2, "singleton returned different instances")
    }

    @Test
    fun `singleton() and newInstance() should return different instances when singleton() is called first`() {
        val fake1: Fake = Provider.singleton()
        val fake2: Fake = Provider.newInstance()

        assertTrue(fake1 != fake2, "singleton() and newInstance() returned same instance")
    }

    @Test
    fun `singleton() and newInstance() should return different instances when newInstance() is called first`() {
        val fake1: Fake = Provider.newInstance()
        val fake2: Fake = Provider.singleton()

        assertTrue(fake1 != fake2, "singleton() and newInstance() returned same instance")
    }

    interface Fake
    class FakeClass : Fake
}