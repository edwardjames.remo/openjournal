package com.aguragorn.openjournal.utils

import com.aguragorn.openjournal.di.Provider
import kotlin.test.Test
import kotlin.test.assertEquals

@Suppress("FunctionName")
class DateTimeTests {
    @Test
    fun `toUserPreferredFormat() returns time of 8am when date time is midnight and offset is plus 08`() {
        Provider.register(id = DateTime.PROVIDER_ID_TIMEZONE) { "+08:00" }
        Provider.register(id = DateTime.PROVIDER_ID_DATE_TIME_FORMAT) { "HH:mm:ss" }

        val dateTime = DateTime.now().apply {
            hour = 0
            minute = 0
            second = 0
            nano = 0
        }

        assertEquals("08:00:00", dateTime.toUserPreferredFormat())
    }
}