package com.aguragorn.openjournal.utils

import kotlin.test.Test
import kotlin.test.assertEquals

@Suppress("FunctionName")
class StringUtilsTests {
    @Test
    fun `randomGuid() should return a different string for each call`() {
        val results = listOf(String.randomGuid(), String.randomGuid(), String.randomGuid())
        val unique = results.distinct()

        assertEquals(results, unique, "some results are duplicated")
    }
}